package sip2;

import com.pkrete.jsip2.connection.SIP2SocketConnection;
import com.pkrete.jsip2.exceptions.InvalidSIP2ResponseException;
import com.pkrete.jsip2.exceptions.InvalidSIP2ResponseValueException;
import com.pkrete.jsip2.messages.requests.SIP2CheckinRequest;
import com.pkrete.jsip2.messages.requests.SIP2CheckoutRequest;
import com.pkrete.jsip2.messages.requests.SIP2FeePaidRequest;
import com.pkrete.jsip2.messages.requests.SIP2ItemInformationRequest;
import com.pkrete.jsip2.messages.requests.SIP2LoginRequest;
import com.pkrete.jsip2.messages.requests.SIP2PatronInformationRequest;
import com.pkrete.jsip2.messages.requests.SIP2SCStatusRequest;
import com.pkrete.jsip2.messages.responses.SIP2ACSStatusResponse;
import com.pkrete.jsip2.messages.responses.SIP2CheckinResponse;
import com.pkrete.jsip2.messages.responses.SIP2CheckoutResponse;
import com.pkrete.jsip2.messages.responses.SIP2FeePaidResponse;
import com.pkrete.jsip2.messages.responses.SIP2ItemInformationResponse;
import com.pkrete.jsip2.messages.responses.SIP2LoginResponse;
import com.pkrete.jsip2.messages.responses.SIP2PatronInformationResponse;

public class SelfCheck {

    public static SIP2SocketConnection connection;

    public static void main(String[] args) throws InvalidSIP2ResponseException, InvalidSIP2ResponseValueException {
        connection = new SIP2SocketConnection("localhost", 6001);
        if (connection.connect()) {
            System.out.println("Connection Sucess");
            SIP2LoginResponse loginResponse = login("123456789", "123456789", "TUPUEY");
            if (loginResponse.isOk()) {
                checkOut("TUPUEY", "123456789", "123456789", "31379004879707");
                //checkIn("TUPUEY", "31379004879707");
                //itemInfo("31379004879707");
                //feePaid("TUPEUY","123456789", "123456789");
            }
        }else{
            System.out.println("Connection fail");
        }
    }

    public static SIP2LoginResponse login(String user, String password, String location) throws InvalidSIP2ResponseException, InvalidSIP2ResponseValueException {

        SIP2LoginRequest login = new SIP2LoginRequest(user, password, location);
        SIP2LoginResponse loginResponse = (SIP2LoginResponse) connection.send(login);
        if (loginResponse.isOk()) {
            System.out.println("Login Sucess");
        } else {
            System.out.println("Login fail");
        }
        return loginResponse;
    }

    public static void checkOut(String institutionId, String patronIdentifier, String patronPassword, String itemIdentifier) throws InvalidSIP2ResponseException, InvalidSIP2ResponseValueException {

        SIP2SCStatusRequest status = new SIP2SCStatusRequest();
        SIP2ACSStatusResponse statusResponse = (SIP2ACSStatusResponse) connection.send(status);

        SIP2PatronInformationRequest request = new SIP2PatronInformationRequest(institutionId, patronIdentifier, patronPassword);
        SIP2PatronInformationResponse response = (SIP2PatronInformationResponse) connection.send(request);

        if (response.isValidPatron() && response.isValidPatronPassword()) {
            System.out.println("Patron Info Valid");
            SIP2CheckoutRequest co = new SIP2CheckoutRequest(patronIdentifier, itemIdentifier);
            SIP2CheckoutResponse coResponse = (SIP2CheckoutResponse) connection.send(co);
            if (coResponse.isOk()) {
                if (coResponse.isRenewalOk()) {
                    System.out.println("Renewal sucess");
                } else {
                    System.out.println("Check Out sucess");
                }
            } else {
                System.out.println("Check Out fail");
            }
        } else {
            System.out.println("Patron Info Invalid");
        }
    }

    public static void checkIn(String institutionId, String itemIdentifier) throws InvalidSIP2ResponseException, InvalidSIP2ResponseValueException {
        SIP2SCStatusRequest status = new SIP2SCStatusRequest();
        SIP2ACSStatusResponse statusResponse = (SIP2ACSStatusResponse) connection.send(status);
        SIP2CheckinRequest request = new SIP2CheckinRequest(institutionId, itemIdentifier);
        SIP2CheckinResponse response = (SIP2CheckinResponse) connection.send(request);
        if (response.isOk()) {
            System.out.println("Check In sucess");
        } else {
            System.out.println("Check In fail");
        }
        System.out.println(response);
    }

    public static void itemInfo(String itemIdentifier) throws InvalidSIP2ResponseException, InvalidSIP2ResponseValueException {
        SIP2SCStatusRequest status = new SIP2SCStatusRequest();
        SIP2ACSStatusResponse statusResponse = (SIP2ACSStatusResponse) connection.send(status);
        SIP2ItemInformationRequest request = new SIP2ItemInformationRequest(itemIdentifier);
        SIP2ItemInformationResponse response = (SIP2ItemInformationResponse) connection.send(request);
        System.out.println("------------------Item Info-------------------------");
        System.out.println("Barcode Book = "+response.getItemIdentifier());
        System.out.println("Title = "+response.getTitleIdentifier());
        System.out.println("Library = "+response.getOwner());
        System.out.println("CurrentLocation = "+response.getCurrentLocation());
        System.out.println("Due Date = "+response.getDueDate());
        System.out.println("Recall Date = "+response.getRecallDate());
        System.out.println("Pickup Location = "+response.getPickupLocation());
        System.out.println("Expiration Date = "+response.getExpirationDate());
        System.out.println("Circulation Status = "+response.getCirculationStatus());
        System.out.println("Security Marker = "+response.getSecurityMarker());
        System.out.println("---------------------------------------------------");
    }

    public static void feePaid(String institutionId, String patronIdentifier, String patronPassword) throws InvalidSIP2ResponseException, InvalidSIP2ResponseValueException {
        SIP2SCStatusRequest status = new SIP2SCStatusRequest();
        SIP2ACSStatusResponse statusResponse = (SIP2ACSStatusResponse) connection.send(status);
        SIP2PatronInformationRequest request = new SIP2PatronInformationRequest(institutionId, patronIdentifier, patronPassword);
        SIP2PatronInformationResponse response = (SIP2PatronInformationResponse) connection.send(request);

        if (response.isValidPatron() && response.isValidPatronPassword()) {
            String fee = "3,50";
            SIP2FeePaidRequest feeRequest = new SIP2FeePaidRequest(patronIdentifier, fee);
            SIP2FeePaidResponse feeResponse = (SIP2FeePaidResponse) connection.send(feeRequest);

            if (feeResponse.isPaymentAccepted()) {
                System.out.println("------------------Item Info-------------------------");
                System.out.println("Barcode Book = " + feeResponse.getItemIdentifier());
                System.out.println("Title = " + feeResponse.getTitleIdentifier());
                System.out.println("Barcode Book = " + feeResponse.getItemIdentifier());
                System.out.println("Barcode Book = " + feeResponse.getItemIdentifier());
                System.out.println("Barcode Book = " + feeResponse.getItemIdentifier());
                System.out.println("fee Amount = " + feeResponse.getFeeAmount());
                
                System.out.println("---------------------------------------------------");
            }
        }
    }

}
