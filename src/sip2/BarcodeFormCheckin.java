package sip2;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import com.pkrete.jsip2.connection.SIP2SocketConnection;
import com.pkrete.jsip2.exceptions.InvalidSIP2ResponseException;
import com.pkrete.jsip2.exceptions.InvalidSIP2ResponseValueException;
import com.pkrete.jsip2.messages.requests.SIP2CheckinRequest;
import com.pkrete.jsip2.messages.requests.SIP2CheckoutRequest;
import com.pkrete.jsip2.messages.requests.SIP2FeePaidRequest;
import com.pkrete.jsip2.messages.requests.SIP2ItemInformationRequest;
import com.pkrete.jsip2.messages.requests.SIP2LoginRequest;
import com.pkrete.jsip2.messages.requests.SIP2PatronInformationRequest;
import com.pkrete.jsip2.messages.requests.SIP2SCStatusRequest;
import com.pkrete.jsip2.messages.responses.SIP2ACSStatusResponse;
import com.pkrete.jsip2.messages.responses.SIP2CheckinResponse;
import com.pkrete.jsip2.messages.responses.SIP2CheckoutResponse;
import com.pkrete.jsip2.messages.responses.SIP2FeePaidResponse;
import com.pkrete.jsip2.messages.responses.SIP2ItemInformationResponse;
import com.pkrete.jsip2.messages.responses.SIP2LoginResponse;
import com.pkrete.jsip2.messages.responses.SIP2PatronInformationResponse;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import static sip2.SelfCheck.connection;
import static sip2.SelfCheck.login;

public class BarcodeFormCheckin extends JPanel {

    private JTextField barcodeField;
    private JFrame ref;
    private JFrame frame = new JFrame("");
    private static String usernameStaff;
    private static String passwordStaff;

    // Create a form with the specified labels, tooltips, and sizes.
    public BarcodeFormCheckin(String u, String pwd) {
        super(new BorderLayout());
        usernameStaff = u;
        passwordStaff = pwd;
        JPanel labelPanel = new JPanel(new GridLayout(1, 1));
        JPanel fieldPanel = new JPanel(new GridLayout(1, 1));
        add(labelPanel, BorderLayout.WEST);
        add(fieldPanel, BorderLayout.CENTER);

        barcodeField = new JTextField();
        barcodeField.setToolTipText("Barcode");
        barcodeField.setColumns(15);

        JLabel lab = new JLabel(" Barcode", JLabel.RIGHT);
        lab.setLabelFor(barcodeField);
        lab.setForeground(Color.BLUE);

        labelPanel.add(lab);
        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        p.add(barcodeField);
        fieldPanel.add(p);

    }

    public JFrame createWindow() {
        JFrame f = null;
        JLabel labelUsername = new JLabel("Check in", SwingConstants.RIGHT);
        labelUsername.setForeground(Color.RED);

        JButton submit = new JButton("Submit");

        submit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                SIP2SocketConnection connection = new SIP2SocketConnection("localhost", 6001);
                if (connection.connect()) {
                    try {

                        SIP2LoginRequest login = new SIP2LoginRequest("123456789", "123456789", "TUPUEY");
                        SIP2LoginResponse loginResponse = (SIP2LoginResponse) connection.send(login);
                        if (loginResponse.isOk()) {
                            SIP2SCStatusRequest status = new SIP2SCStatusRequest();
                            SIP2ACSStatusResponse statusResponse = (SIP2ACSStatusResponse) connection.send(status);
                            SIP2ItemInformationRequest requestItem = new SIP2ItemInformationRequest(barcodeField.getText());
                            SIP2ItemInformationResponse responseItem = (SIP2ItemInformationResponse) connection.send(requestItem);
                            String out = "------------------Item Info-------------------------\n";
                            out += "Barcode Book = " + responseItem.getItemIdentifier() + "\n";
                            out += "Title = " + responseItem.getTitleIdentifier() + "\n";
                            out += "Library = " + responseItem.getOwner() + "\n";
                            out += "CurrentLocation = " + responseItem.getCurrentLocation() + "\n";
                            out += "Due Date = " + responseItem.getDueDate() + "\n";
                            out += "Recall Date = " + responseItem.getRecallDate() + "\n";
                            out += "Pickup Location = " + responseItem.getPickupLocation() + "\n";
                            out += "Expiration Date = " + responseItem.getExpirationDate() + "\n";
                            out += "Circulation Status = " + responseItem.getCirculationStatus() + "\n";
                            out += "Security Marker = " + responseItem.getSecurityMarker() + "\n";
                            out += "---------------------------------------------------\n";
                            JOptionPane.showMessageDialog(frame, out);
                            SIP2CheckinRequest request = new SIP2CheckinRequest("TUPUEY", barcodeField.getText());
                            SIP2CheckinResponse response = (SIP2CheckinResponse) connection.send(request);
                            if (response.isOk()) {
                                JOptionPane.showMessageDialog(frame, "Check in sucess");
                            } else {
                                JOptionPane.showMessageDialog(frame, "Check in fail");
                            }

                            barcodeField.setText("");
                            connection.close();
                        }
                    } catch (InvalidSIP2ResponseException | InvalidSIP2ResponseValueException ex) {
                        Logger.getLogger(BarcodeFormCheckin.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
        );
        JButton mainMenu = new JButton("Main menu");

        mainMenu.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                ref.dispose();
                // --------------------------
                MainMenu mainMenu = new MainMenu(usernameStaff, passwordStaff);
                JFrame mainMenuRef = mainMenu.createWindow();
            }
        });

        f = new JFrame("Check in");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.add(labelUsername);
        f.getContentPane().add(labelUsername, BorderLayout.NORTH);
        f.getContentPane().add(this, BorderLayout.CENTER);
        JPanel p = new JPanel();
        p.add(submit);
        p.add(mainMenu);
        f.getContentPane().add(p, BorderLayout.SOUTH);
        f.pack();
        f.setLocationRelativeTo(null);
        f.setVisible(true);
        ref = f;
        return f;
    }

}
