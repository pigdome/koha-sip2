package sip2;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.Dimension;

public class MainMenu extends JPanel {

  
  private JFrame ref;
  private String usernameStaff;
  private String passwordStaff;
  // Create a form with the specified labels, tooltips, and sizes.
  public MainMenu(String username,String password) 
  {
    super(new BorderLayout());
    usernameStaff = username;
    passwordStaff = password;
    JPanel btnPanel = new JPanel(new GridLayout(2, 1));
    add(btnPanel, BorderLayout.NORTH);
    

    // Check in Button
    JButton checkinButton = new JButton("Check in");
    checkinButton.setPreferredSize(new Dimension(200, 80));
    checkinButton.addActionListener( new ActionListener()
    {
       public void actionPerformed(ActionEvent e)
       {
          // call method from kitkang
          
          ref.dispose();
          BarcodeFormCheckin bcf = new BarcodeFormCheckin(usernameStaff,passwordStaff);
          bcf.createWindow();
       }
    } );
    btnPanel.add(checkinButton);

    JButton checkoutButton = new JButton("Check out");
    checkoutButton.setPreferredSize(new Dimension(200, 80));
    checkoutButton.addActionListener( new ActionListener()
    {
       public void actionPerformed(ActionEvent e)
       {
          // call method from kitkang
          ref.dispose();
          LoginPatron lp = new LoginPatron(usernameStaff,passwordStaff);
          lp.createWindow();
       }
    } );
    btnPanel.add(checkoutButton);
  }


  public JFrame createWindow() 
  {
    JFrame f = null;
    f = new JFrame("MainMenu");
    f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    f.getContentPane().add(this, BorderLayout.NORTH);
    //f.setSize(500,500);
    f.pack();
    f.setLocationRelativeTo(null);
    f.setVisible(true);
    ref = f;
    return f;         
  }
}