package sip2;

import com.pkrete.jsip2.connection.SIP2SocketConnection;
import com.pkrete.jsip2.exceptions.InvalidSIP2ResponseException;
import com.pkrete.jsip2.exceptions.InvalidSIP2ResponseValueException;
import com.pkrete.jsip2.messages.requests.SIP2LoginRequest;
import com.pkrete.jsip2.messages.requests.SIP2PatronInformationRequest;
import com.pkrete.jsip2.messages.requests.SIP2SCStatusRequest;
import com.pkrete.jsip2.messages.responses.SIP2ACSStatusResponse;
import com.pkrete.jsip2.messages.responses.SIP2LoginResponse;
import com.pkrete.jsip2.messages.responses.SIP2PatronInformationResponse;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import java.awt.event.KeyEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;


public class LoginPatron extends JPanel {

    private static JTextField userField;
    private static JPasswordField passField;
    public static JFrame ref = null;
    private static String usernameStaff;
    private static String passwordStaff;
    private static JFrame frame = new JFrame("");
    // Create a form with the specified labels, tooltips, and sizes.
    public LoginPatron(String u,String pwd) {
        super(new BorderLayout());
        usernameStaff = u;
        passwordStaff = pwd;
        JPanel labelPanel = new JPanel(new GridLayout(2, 1));
        JPanel fieldPanel = new JPanel(new GridLayout(2, 1));
        add(labelPanel, BorderLayout.WEST);
        add(fieldPanel, BorderLayout.CENTER);

        // Username Field
        userField = new JTextField();
        userField.setToolTipText("Username");
        userField.setColumns(15);

        JLabel userlab = new JLabel(" Username", JLabel.RIGHT);
        userlab.setLabelFor(userField);
        userlab.setForeground(Color.BLUE);

        labelPanel.add(userlab);
        JPanel userpanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        userpanel.add(userField);
        fieldPanel.add(userpanel);

        // Password Field
        passField = new JPasswordField(15);
        passField.setToolTipText("Password");

        JLabel passLab = new JLabel(" Password", JLabel.RIGHT);
        passLab.setLabelFor(passField);
        passLab.setForeground(Color.BLUE);

        labelPanel.add(passLab);
        JPanel passpanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        passpanel.add(passField);
        fieldPanel.add(passpanel);

    }

    public JFrame createWindow() {
        JButton submit = new JButton("Submit");

        submit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    checkPasswd();
                } catch (InvalidSIP2ResponseException ex) {
                    Logger.getLogger(LoginPatron.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InvalidSIP2ResponseValueException ex) {
                    Logger.getLogger(LoginPatron.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    try {
                        checkPasswd();
                    } catch (InvalidSIP2ResponseException ex) {
                        Logger.getLogger(LoginPatron.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (InvalidSIP2ResponseValueException ex) {
                        Logger.getLogger(LoginPatron.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });

        JFrame f = null;
        f = new JFrame("Login Patron");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.getContentPane().add(this, BorderLayout.NORTH);
        JPanel p = new JPanel();
        p.add(submit);
        f.getContentPane().add(p, BorderLayout.SOUTH);
        f.pack();
        f.setLocationRelativeTo(null);
        f.setVisible(true);
        ref = f;

        return f;

    }

    public static void checkPasswd() throws InvalidSIP2ResponseException, InvalidSIP2ResponseValueException {
        String username = "";
        String password = "";
        username = userField.getText();
        password = passField.getText();
        
        // Check Authen using Koha(SIP)
        SIP2SocketConnection connection = new SIP2SocketConnection("localhost", 6001);
        
        if (connection.connect()) 
        {
            SIP2LoginRequest login = new SIP2LoginRequest(usernameStaff, passwordStaff, "TUPUEY");
            SIP2LoginResponse loginResponse = (SIP2LoginResponse) connection.send(login);
            if (loginResponse.isOk()) 
            {
                
                SIP2SCStatusRequest status = new SIP2SCStatusRequest();
                SIP2ACSStatusResponse statusResponse = (SIP2ACSStatusResponse) connection.send(status);

                SIP2PatronInformationRequest request = new SIP2PatronInformationRequest("TUPUEY", username, password);
                SIP2PatronInformationResponse response = (SIP2PatronInformationResponse) connection.send(request);

                if (response.isValidPatron() && response.isValidPatronPassword()) 
                {
                    connection.close();
                    ref.dispose();
                    BarcodeFormCheckout bfco = new BarcodeFormCheckout(usernameStaff,passwordStaff,username,password);
                    bfco.createWindow();
                } else 
                {
                    JOptionPane.showMessageDialog(frame,"Username or Password incorrect");
                    connection.close();
                    userField.setText("");
                    passField.setText("");
                }
            }
            else
            {
                connection.close();
                ref.dispose();
                LoginPatron lp = new LoginPatron(usernameStaff,passwordStaff);
                lp.createWindow();
            }
        } 
        else 
        {
            System.out.println("kvy");
        }

    // if ok close old window
    }
}
